const s = self as any;

const cacheName = "offline:" + url(".");

function url(file: string){
	return new URL(file, s.location).toString();
}

const urls = [
	".",
	"index.html",
	"main.css",
	"main.js",
	"manifest.webmanifest",
	"icon.svg",
	"dummy.png"
].map(url);

s.addEventListener("install", (e: any) => {
	e.waitUntil((async () => {
		const cache = await s.caches.open(cacheName);
		await cache.addAll(urls);
		s.skipWaiting();
	})());
});

s.addEventListener("fetch", (e: any) => {
	// unhandled
	if(urls.indexOf(e.request.url) < 0)
		return;

	e.respondWith((async () => {
		const cache = await s.caches.open(cacheName);
		try{
			const r = await fetch(e.request.url);
			await cache.put(e.request.url, r.clone());
			return r;
		}catch(err){
			const r = await cache.match(e.request.url);
			if(r)
				return r;
			throw err;
		}
	})());
});
