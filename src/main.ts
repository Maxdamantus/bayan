const colDist = 3;
const rows = 5;
const cols = 15;

const a4freq = 440;

const note0freq = a4freq*Math.pow(2, 3/12); // note0 = C5
const noteNames12tet = ["C", "C♯", "D", "D♯", "E", "F", "F♯", "G", "G♯", "A", "A♯", "B"];

const noteCount = cols*colDist;
const minNote = -2; // alignment such that C5 falls on 'Q'
const maxNote = minNote + noteCount;

type Note = number;

function noteFreq(note: Note): number {
	return note0freq/8*Math.pow(2, note/12);
}

function arrayFrom<T>(length: number, fn: (i: number) => T): T[] {
	const o = [];
	for(let x = 0; x < length; x++)
		o.push(fn(x));
	return o;
}

function modulo(a: number, b: number): number {
	return (a%b + b)%b;
}

export class Keyboard {
	containers: { [note: number]: HTMLElement[] } = Object.create(null);
	noteStates: number[] = arrayFrom(noteCount, _ => 0);
	handleState: (note: Note, on: boolean) => void;

	constructor(handleState: (note: Note, on: boolean) => void){
		this.handleState = handleState;
	}

	addContainer(note: Note, elem: HTMLElement){
		(this.containers[note - minNote] || (this.containers[note - minNote] = [])).push(elem);
	}

	addState(row: number, col: number, d: number){
		const note = this.buttonNote(row, col);
		if(note < minNote || note >= maxNote)
			return;

		const wasDown = this.noteStates[note - minNote] != 0;
		const isDown = (this.noteStates[note - minNote] += d) != 0;
		if(wasDown != isDown){
			const { handleState } = this;
			handleState(note, isDown);
			const containers = this.containers[note - minNote];
			if(containers !== undefined)
				for(const c of containers)
					c.className = isDown? "down" : "";
		}
	}

	keyboardElem(): HTMLElement {
		const rowsElem = document.createElement("div");
		const rowElems = [];
		for(let row = 0; row < rows; row++){
			const rowElem = rowsElem.appendChild(document.createElement("div"));
			const colElems = [];
			colElems.push(e("div", ["offset" + row%colDist], []));
			for(let col = 0; col < cols; col++)
				colElems.push(this.buttonElem(row, col));
			rowElems.push(e("div", ["row"], colElems));
		}
		return e("div", ["keyboard"], rowElems);
	}

	noteName(n: Note): string {
		return noteNames12tet[modulo(n, 12)];
	}

	buttonNote(row: number, col: number): Note {
		return row%colDist + col*3 + minNote;
	}

	keyElem(note: Note): HTMLElement {
		const name = this.noteName(note);
		const diatonicClass = name.length == 1?
			"natural" :
			"accidental";
		return e("div", ["key", diatonicClass], [
			e("div", [], [document.createTextNode(this.noteName(note))]),
			e("div", [], [document.createTextNode(String(note))])
		]);
	}

	buttonElem(row: number, col: number): Node {
		const note = this.buttonNote(row, col);
		const container = e("span", [], [
			this.keyElem(note)
		]);
		this.addContainer(note, container);
		container.ontouchstart = ev => {
			ev.preventDefault();
			this.addState(row, col, 1);
		};
		container.ontouchend = ev => {
			ev.preventDefault();
			this.addState(row, col, -1);
		};
		return container;
	}

	setTranspose(transpose: number){
		for(let x in this.containers){
			const containers = this.containers[x];
			const note: Note = Number(x) + minNote + transpose;
			for(const c of containers){
				while(c.firstChild != null)
					c.removeChild(c.firstChild);
				c.appendChild(this.keyElem(note));
			}
		}
	}
}

type Synth = {
	state(note: Note, on: boolean): void;
	close(): void;
	setTranspose(n: number): void;
};

function makeOscillatorSynth(): Synth {
	const fadeTime = 0.01;
	const ctx = new AudioContext();
	const wave = makeWave(ctx);
	const masterGain = ctx.createGain();
	masterGain.connect(ctx.destination);

	function calcNote(idx: number): Note {
		return idx - minNote;
	}

	const nodes = arrayFrom(noteCount, idx => {
		const note: Note = calcNote(idx);
		const osc = ctx.createOscillator();
		const powerGain = ctx.createGain();
		const gainNode = ctx.createGain();
		osc.frequency.value = noteFreq(note);
		osc.setPeriodicWave(wave);
		osc.connect(powerGain);
		powerGain.connect(gainNode);
		powerGain.gain.value = noteFreq(0)/noteFreq(note);
		gainNode.connect(masterGain);
		gainNode.gain.value = 0;
		osc.start(ctx.currentTime + 0.1*Math.random());
		return {
		    gainNode,
		    osc
		};
	});

	let liveNotes = 0;

	return {
		state(note: Note, on: boolean){
			if(note > nodes.length)
				return;
			liveNotes += on? 1 : -1;

			const node = nodes[note - minNote].gainNode;
			node.gain.cancelScheduledValues(ctx.currentTime);
			node.gain.setValueAtTime(node.gain.value, ctx.currentTime);
			node.gain.linearRampToValueAtTime(on? 1 : 0, ctx.currentTime + fadeTime);

			masterGain.gain.cancelScheduledValues(ctx.currentTime);
			masterGain.gain.setValueAtTime(masterGain.gain.value, ctx.currentTime);
			masterGain.gain.linearRampToValueAtTime(1/(liveNotes + 1), ctx.currentTime + fadeTime);
		},
		setTranspose(n: number){
			for(let x = 0; x < nodes.length; x++)
				nodes[x].osc.frequency.value = noteFreq(calcNote(x + n));
		},
		close(){
			ctx.close();
		}
	};
}

function makeMidiSynth(output: WebMidi.MIDIOutput) {
	return (): Synth => {
		let transpose = 0;
		return {
			state(note: Note, on: boolean){
				output.send([on? 0x90 : 0x80, note + 60 + transpose, 100]);
			},
			setTranspose(n: number){
				transpose = n;
			},
			close(){}
		};
	};
}

function makeWave(ctx: AudioContext): PeriodicWave {
	const len = 10;
	const nodes: [number, number][] = arrayFrom(len, idx =>
		[1/Math.pow(idx + 1, 2), 1/(idx + 1)]
	);
	return ctx.createPeriodicWave(
		Float32Array.from(nodes.map(([a, b]) => a)),
		Float32Array.from(nodes.map(([a, b]) => b))
	);
}

function keyNames(prefix: string, chars: string): string[] {
	return chars.split("").map(c => prefix + c);
}

type KeyHandler = {
	onNote(row: number, col: number, on: boolean): void;
	onTranspose(inc: number): void;
};

function watchKeys({ onNote, onTranspose }: KeyHandler){
	const keys: { [name: string]: { row: number, col: number, down: boolean } } = Object.create(null);
	{
		const rows = [
			[],
			[...keyNames("Digit", "1234567890"), "Minus", "Equal", "Backspace"],
			[...keyNames("Key", "QWERTYUIOP"), "BracketLeft", "BracketRight", "Backslash"],
			[null, ...keyNames("Key", "ASDFGHJKL"), "Semicolon", "Quote", "Enter"],
			[null, ...keyNames("Key", "ZXCVBNM"), "Comma", "Period", "Slash", "ShiftRight"]
		];
		for(let row = 0; row < rows.length; row++){
			const cols = rows[row];
			for(let col = 0; col < cols.length; col++){
				const key = cols[col];
				if(key != null)
					keys[key] = { row, col, down: false };
			}
		}
	}

	const eventKeys: { [name: string]: () => void } = {
		"ArrowDown": () => { onTranspose(-1); },
		"ArrowUp": () => { onTranspose(1); },
		"ArrowLeft": () => { onTranspose(-12); },
		"ArrowRight": () => { onTranspose(12); }
	};

	document.addEventListener("keydown", e => {
		const f = eventKeys[e.code];
		if(f != undefined){
			e.preventDefault();
			f();
			return;
		}

		const o = keys[e.code];
		if(o == undefined)
			return;
		e.preventDefault();
		const { row, col, down } = o;
		if(!down){
			o.down = true;
			onNote(row, col, true);
		}
	});

	document.addEventListener("keyup", e => {
		const o = keys[e.code];
		if(o == undefined)
			return;
		e.preventDefault();
		const { row, col, down } = o;
		if(down){
			o.down = false;
			onNote(row, col, false);
		}
	});
}

type Option = { name: string, action: () => void };

function select(options: Option[]){
	const s = e("select", [], options.map(({ name }) =>
		e("option", [], [document.createTextNode(name)])
	)) as HTMLSelectElement;
	s.addEventListener("change", e => {
		e.preventDefault();
		for(const { name, action } of options){
			if(s.value == name){
				action();
				break;
			}
		}
	});
	return s;
}

function e(tagName: string, classes: string[], children: Node[]): HTMLElement {
	const elem = document.createElement(tagName);
	elem.className = classes.join(" ");
	for(const c of children)
		elem.appendChild(c);
	return elem;
}

function main(){
	let makeSynth = makeOscillatorSynth;
	// can't create it initially because we require a user interaction
	let synth: Synth | null = null;
	let selectElem: HTMLElement | null = null;
	let transposeElem: HTMLInputElement | null = null;
	let keyboardElem: HTMLElement | null = null;
	let fullscreenElem: HTMLButtonElement | null = null;
	let installContainerElem: HTMLElement | null = null;
	let transpose = 0;

	const options: Option[] = [{ name: "Oscillators", action(){ makeSynth = makeOscillatorSynth; reset(); } }];
	if(navigator.requestMIDIAccess)
		options.push({
			name: "MIDI",
			action(){
				navigator.requestMIDIAccess()
					.then((access) => {
						const midiOptions: Option[] = [];
						for(const output of access.outputs.values())
							midiOptions.push({
								name: "MIDI (" + output.name + ")",
								action(){
									makeSynth = makeMidiSynth(output);
									reset();
								}
							});
						const newSelectElem = select([...midiOptions, ...options]);
						selectElem!.parentNode!.replaceChild(newSelectElem, selectElem!);
						selectElem = newSelectElem;
						for(const output of access.outputs.values()){
							makeSynth = makeMidiSynth(output);
							reset();
							return;
						}
					});
			}
		});

	function reset() {
		synth = makeSynth();
		synth.setTranspose(transpose);
	}

	const keyboard = new Keyboard((note, on) => {
		if(synth == null)
			reset();
		synth!.state(note, on);
	});

	function setTranspose(transpose: number){
		keyboard.setTranspose(transpose);
		if(synth != null)
			synth.setTranspose(transpose);
	}

	document.body.appendChild(selectElem = select(options));
	document.body.appendChild(document.createTextNode("Transpose: "));
	document.body.appendChild(transposeElem = document.createElement("input")).type = "number";
	document.body.appendChild(fullscreenElem = document.createElement("button")).appendChild(document.createTextNode("⛶"));
	document.body.appendChild(installContainerElem = document.createElement("span"));
	document.body.appendChild(keyboardElem = keyboard.keyboardElem());

	fullscreenElem.onclick = () => {
		keyboardElem!.requestFullscreen();
	};

	transposeElem.value = String(transpose);
	transposeElem.onchange = () => {
		transpose = Number(transposeElem!.value);
		setTranspose(transpose);
	};

	watchKeys({
		onNote(row, col, on){
			keyboard.addState(row, col, on? 1 : -1);
		},
		onTranspose(inc: number){
			const transpose = Number(transposeElem!.value) + inc;
			transposeElem!.value = String(transpose);
			setTranspose(transpose);
		}
	});

	if("serviceWorker" in navigator){
		navigator.serviceWorker.register("sw.js");
	}

	window.addEventListener("beforeinstallprompt", e => {
		e.preventDefault();
		while(installContainerElem!.firstChild != null)
			installContainerElem!.removeChild(installContainerElem!.firstChild);
		const b = installContainerElem!.appendChild(document.createElement("button"));
		b.appendChild(document.createTextNode("Install PWA"));
		b.onclick = () => {
			(e as any).prompt();
		};
	});
}

function onLoad(f: () => void){
	document.addEventListener("DOMContentLoaded", f);
}

onLoad(main);
